from python:3.7.1-alpine3.7
run apk add --no-cache git
run apk add --no-cache gcc musl-dev python-dev libffi-dev openssl-dev
workdir /bot
add requirements.txt .
run pip install -r requirements.txt
add . .
workdir /bot/src
cmd python3 main.py