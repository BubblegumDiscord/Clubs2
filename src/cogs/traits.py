import discord, datetime
from discord.ext import commands
from static_data.data import clubs, Club
from dynamic.switching_disabled import can_switch

class FlavorsCog:
    def __init__(self, bot):
        self.bot = bot

    def get_club(self, m: discord.Member):
        """
        Returns the Club of a member.
        """

        for club in clubs:
            if club.role in [r.id for r in m.roles]:
                return club
    def get_current_order(self, m: discord.Member):
        club = self.get_club(m)
        found = None
        for trait in club.traits:
            

    @commands.command()
    @commands.guild_only()
    async def traits(self, ctx):
        # Set current_club to their Club
        current_club = self.get_club(ctx.message.author)
        if not current_club:
            # Or if they aint in a club...
            await ctx.send(
                embed = discord.Embed(
                    title = "You're not in a club!",
                    color = ctx.message.author.color,
                    description = "You could try " + (", or ".join([("!join " + x.name) for x in clubs]))
                )
            )
            return


        embed = discord.Embed(
            title = "Available flavors:",
            description = "mmm tasty",
            timestamp = datetime.datetime.now(),
            color = ctx.message.author.color
        ).set_author(
            name=str(ctx.message.author),
            icon_url=ctx.message.author.avatar_url
        )

        # Loop flavors, adding to embed
        for flavor in current_club.flavors:
            embed.add_field(name = ctx.message.guild.get_role(flavor.role), value = f"<@&{flavor.role}>")
        
        # TODO: BLANK FIELD

        # Add explainer field
        embed.add_field(
            name = "To join a flavor:",
            value = "just type `!flavor ` then part of the name of the flavor you want.",
            inline = False
        )

        await ctx.send(
            embed = embed
        )

    @commands.command()
    @commands.guild_only()
    async def flavor(self, ctx, *, role: discord.Role):
        # Set current_club to their Club
        current_club = self.get_club(ctx.message.author)
        if not current_club:
            # Or if they aint in a club...
            await ctx.send(
                embed = discord.Embed(
                    title = "You're not in a club!",
                    color = ctx.message.author.color,
                    description = "You could try " + (", or ".join([("!join " + x.name) for x in clubs]))
                )
            )
            return

        # Check role is a flavor role
        if role.id not in [f.role for f in current_club.flavors]:
            await ctx.send(
                embed = discord.Embed(
                    title = "That's not a flavor <:gigl:426979065082150912>",
                    color = ctx.message.author.color,
                    description = "You could try having a look at `!flavors` to see what's available."
                ).set_author(
                    name=str(ctx.message.author),
                    icon_url=ctx.message.author.avatar_url
                )
            )
            return

        # If so, remove all flavor roles

        # That list comprehension is a speed hack to only remove roles they have
        for flavor in [x for x in current_club.flavors if x.role in [r.id for r in ctx.message.author.roles]]:
            try: await ctx.message.author.remove_roles(discord.Object(flavor.role))
            except discord.HTTPException: pass

        # And add the correct flavor role
        await ctx.message.author.add_roles(discord.Object(role.id))

        # Tell them
        await ctx.send(
            embed = discord.Embed(
                title = f"You've been given the {role.name} flavor <:pepefedora:371751937298923531>",
                color = role.color,
                description = ":ok_hand:. To remove it, join another flavor or use `!removeflavor {role.name}`."
            ).set_author(
                name=str(ctx.message.author),
                icon_url=ctx.message.author.avatar_url
            )
        )
        
    @commands.command()
    @commands.guild_only()
    async def removeflavor(self, ctx, *, role: discord.Role):
        # Set current_club to their Club
        current_club = self.get_club(ctx.message.author)
        if not current_club:
            # Or if they aint in a club...
            await ctx.send(
                embed = discord.Embed(
                    title = "You're not in a club!",
                    color = ctx.message.author.color,
                    description = "You could try " + (", or ".join([("!join " + x.name) for x in clubs]))
                )
            )
            return

        # Check role is a flavor role
        if role.id not in [f.role for f in current_club.flavors]:
            await ctx.send(
                embed = discord.Embed(
                    title = "That's not a flavor <:gigl:426979065082150912>",
                    color = ctx.message.author.color,
                    description = "You could try having a look at `!flavors` to see what's available."
                ).set_author(
                    name=str(ctx.message.author),
                    icon_url=ctx.message.author.avatar_url
                )
            )
            return

        # If so, remove the role
        await ctx.message.author.remove_roles(discord.Object(role.id))

        # And tell em
        await ctx.send(
            embed = discord.Embed(
                title = f"I've removed the {role.name} flavor <:pepefedora:371751937298923531>",
                color = role.color,
                description = "*It's like it was never there*"
            ).set_author(
                name=str(ctx.message.author),
                icon_url=ctx.message.author.avatar_url
            )
        )

# The setup fucntion below is neccesarry. Remember we give bot.add_cog() the name of the class in this case FlavorsCog.
# When we load the cog, we use the name of the file.
def setup(bot):
    #bot.add_cog(FlavorsCog(bot))