import discord
from discord.ext import commands
from peewee import *

def get_secret(name: str):
    with open("secrets/" + name) as f: return f.read()

db = MySQLDatabase(
    database = get_secret("sql_database"),
    user = get_secret("sql_user"),
    password = get_secret("sql_password"),
    host = get_secret("sql_ip"),
)

class Role(Model):
    owner_id = CharField(50)
    role_id = CharField(50)
    class Meta:
        database = db
        table_name = "sharablerole"

class RolesCog:
    def __init__(self, bot):
        bot.database = db
        self.bot = bot
        
        bot.database.create_tables([Role])
        

    @commands.group()
    async def role(self, ctx): pass

    @role.command()
    async def give(self, ctx, member: discord.Member, role: discord.Role):
        '''
        Shares a sharable role with a member
        '''
        
        try:
            r = Role.get(
                (Role.owner_id == ctx.message.author.id) & 
                (Role.role_id == role.id)
            )
        except: # If role doesn't exist
            await ctx.send(embed = discord.Embed(
                title = "You don't own that role <:blank:454280878353416192>",
                color = discord.Color(0x22aed1),
            ))
            return
        
        # Give it to their friend
        await member.add_roles(role)

        await ctx.send(embed = discord.Embed(
            title = "Done <a:windup:433153386859200522>",
            color = discord.Color(0x22aed1),
            description = f"You can use that same command with `take` instead of `give` to take the role; {ctx.message.author.name} giveth and {ctx.message.author.name} taketh away."
        ))
        

    @role.command()
    async def take(self, ctx, member: discord.Member, role: discord.Role):
        '''
        Takes a sharable role from a member
        '''
        
        try:
            r = Role.get(
                (Role.owner_id == ctx.message.author.id) & 
                (Role.role_id == role.id)
            )
        except: # If role doesn't exist
            await ctx.send(embed = discord.Embed(
                title = "You don't own that role <:blank:454280878353416192>",
                color = discord.Color(0x22aed1),
            ))
            return
        
        # Give it to their friend
        await member.remove_roles(role)

        await ctx.send(embed = discord.Embed(
            title = "Done <a:windup:433153386859200522>",
            color = discord.Color(0x22aed1),
            description = f"You can use that same command with `give` instead of `take` to give the role back; {ctx.message.author.name} giveth and {ctx.message.author.name} taketh away."
        ))
        



    @commands.group()
    async def roleadmin(self, ctx):
        pass

    @roleadmin.command()
    @commands.has_permissions(administrator=True)
    async def add(self, ctx, member: discord.Member, role: discord.Role):
        '''
        Adds a sharable role to a member
        '''
        
        already = bool(Role.select().where(
            (Role.owner_id == member.id) & 
            (Role.role_id == role.id)).count())
        if already:
            await ctx.send(embed = discord.Embed(
                title = "They already own that role <a:drinkingbleach:438653234345803776>",
                color = discord.Color(0x22aed1),
            ))
            return

        r = Role.create(owner_id = member.id, role_id = role.id)
        await ctx.send(embed = discord.Embed(
            title = "That's now sharable <a:plzfrog:406235924876361728>",
            color = discord.Color(0x22aed1),
            description = f"{member.mention} can now use `!role give @TheirFriend {role.name}` to give their role, or the same command with `take` to... uh... take it."
        ))
        

    @roleadmin.command()
    @commands.has_permissions(administrator=True)
    async def remove(self, ctx, member: discord.Member, role: discord.Role):
        '''
        Takes a sharable role from a member
        '''
        
        try:
            r = Role.get(
                (Role.owner_id == member.id) & 
                (Role.role_id == role.id)
            )
        except: # If role doesn't exist
            await ctx.send(embed = discord.Embed(
                title = "They don't own that role <a:drinkingbleach:438653234345803776>",
                color = discord.Color(0x22aed1),
            ))
            return
        # Delete it, bye hunny!
        r.delete_instance()

        # UI <3
        await ctx.send(embed = discord.Embed(
            title = "That's no longer sharable <a:plzfrog:406235924876361728>",
            color = discord.Color(0x22aed1),
            description = f"{member.mention} no longer owns that."
        ).set_footer(text = "*You snatched their weave*"))
        
    

# The setup fucntion below is neccesarry. Remember we give bot.add_cog() the name of the class in this case ClubsCog.
# When we load the cog, we use the name of the file.
def setup(bot):
    bot.add_cog(RolesCog(bot))