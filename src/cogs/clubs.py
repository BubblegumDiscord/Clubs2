import discord
from discord.ext import commands
from static_data.data import clubs, Club
from dynamic.switching_disabled import can_switch

class ClubsCog:
    def __init__(self, bot):
        self.bot = bot

    def get_club(self, m: discord.Member):
        """
        Returns the Club of a member.
        """

        for club in clubs:
            if club.role in [r.id for r in m.roles]:
                return club

    @commands.command()
    @commands.guild_only()
    async def join(self, ctx, club: str):
        """
        Joins a club
        """

        club_found: Club = None
        for club_iter in clubs:
            if (club_iter.name == club) or (club_iter.club_id == club):
                club_found: Club = club_iter
        if not club_found:
            await ctx.send(embed = discord.Embed(
                title = "Club not found 😭",
                color = ctx.message.author.color,
                description = "You could try " + (", or ".join([("!join " + x.name) for x in clubs]))
            ))
            return

        # Person defo has a valid club
        if not can_switch():
            await ctx.send(embed = discord.Embed(
                title = "Club switching is disabled at the moment",
                color = ctx.message.author.color,
                description = "Check <#334794306898493450> or <#408451765344796675> for more information."
            ))
            return
        
        # Switching is enabled

        # Tell them it's finished, just to make it seem fast :HYPERREE:
        await ctx.send(
            embed = discord.Embed(
                title = f"You are now part of the {club_found.name} club!",
                color = ctx.message.author.color,
                description = "fancy!"
            ).add_field(
                name = "To see the different flavors:",
                value = "just type `!flavors` and you'll see a list and instructions to join them - to see all available traits just type `!traits`!"
            ).set_author(
                name=str(ctx.message.author),
                icon_url=ctx.message.author.avatar_url
            )
        )

        # Remove 'Sugar Free'
        try: await ctx.message.author.remove_roles(discord.Object(334803162030407680))
        except discord.HTTPException: pass

        current_club: Club = self.get_club(ctx.message.author)
        if current_club:
            # That list comprehension is a speed hack to only remove roles they have
            for flavor in [x for x in current_club.flavors if x.role in [r.id for r in ctx.message.author.roles]]:
                try: await ctx.message.author.remove_roles(discord.Object(flavor.role))
                except discord.HTTPException: pass

            # TODO: Traits
            await ctx.message.author.remove_roles(discord.Object(current_club.role))

        # Add role of club to switch to
        await ctx.message.author.add_roles(discord.Object(club_found.role))

    

# The setup fucntion below is neccesarry. Remember we give bot.add_cog() the name of the class in this case ClubsCog.
# When we load the cog, we use the name of the file.
def setup(bot):
    bot.add_cog(ClubsCog(bot))