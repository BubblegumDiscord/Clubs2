from discord.ext import commands
import sys, traceback, discord
from peewee import *

def get_secret(name: str):
    with open("secrets/" + name) as f: return f.read()

bot = commands.Bot(command_prefix='$')

initial_extensions = [
    "cogs.roles"
]

if __name__ == '__main__':
    for extension in initial_extensions:
        try:
            bot.load_extension(extension)
        except Exception as e:
            print(f'Failed to load extension {extension}.', file=sys.stderr)
            traceback.print_exc()

@bot.event
async def on_ready():
    print("Connected to discord!")

bot.run(get_secret("token"))