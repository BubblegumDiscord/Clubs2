from .classes import *

# GUM

gum_girl = ClubGirl("Eva", "https://cdn.discordapp.com/attachments/334794306898493450/438457965712900116/Gum_Club_Girl_vr2.jpg")
gum_flavors = [Flavor(x) for x in [
    434069826688778251,
    434070042695696415,
    434070303442862102,
    434070352910614538,
    445628601656672268,
    434070456824365056,
    434070500608835584
]]
gum = Club("gum", "Gum", 414380333669285888, gum_girl)
for flavor in gum_flavors: gum.add_flavor(flavor)
gum_traits = [
    [451357495802462220, 2000],
    [451374568527888414, 4000],
    [451376642829320202, 8000],
    [451376664492900354, 12000],
    [451376669199171594, 0]
]
gum_traits = [Trait(x[0], x[1], gum) for x in gum_traits]
for trait in gum_traits: gum.add_trait(trait)

# LOLLIPOP
lollipop_girl = ClubGirl("Illia", "https://cdn.discordapp.com/attachments/334794306898493450/438458059984076800/Lollipop_Club_Girl.jpg")
lollipop_flavors = [Flavor(x) for x in [
    434070563984506900,
    434070611044728834,
    434070690015084547,
    434070754020294657,
    434070803848364042,
    434070871217537025,
    434070921943318531
]]
lollipop = Club("lollipop", "Lollipop", 414380672698810368, lollipop_girl)
for flavor in lollipop_flavors: lollipop.add_flavor(flavor)
lollipop_traits = [
    [451376905242017832, 2000],
    [451376907758600202, 4000],
    [451376910434435072, 8000],
    [451376930063777792, 12000],
    [451376932228038656, 0]
]
lollipop_traits = [Trait(x[0], x[1], lollipop) for x in lollipop_traits]
for trait in lollipop_traits: lollipop.add_trait(trait)

clubs: list = [lollipop, gum]