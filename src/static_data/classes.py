class ClubGirl:
    """
    A class to store a club girl and her picture
    """

    def __init__(self, name: str, image: str):
        self.name = name
        self.image = image


class Flavor:
    """
    A class to store a single Flavor
    """

    def __init__(self, role: int):
        self.role = role

class Trait(Flavor):
    """
    A class to store a club Trait
    """

    def __init__(self, role: int, cost: int, club):
        self.role = role
        self.cost = cost
        self.club = club

class Club:
    """
    A class to store a single club
    """

    def __init__(self, club_id: str, name: str, role: int, girl):
        self.club_id = club_id
        self.name = name
        self.role = role
        self.girl = girl
        self.flavors = []
        self.traits = []

    def add_flavor(self, flavor: Flavor):
        self.flavors += [flavor]
    def add_trait(self, trait):
        self.traits += [trait]
