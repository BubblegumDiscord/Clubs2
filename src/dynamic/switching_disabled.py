def can_switch():
    try:  
        with open("./dynamic/switching_enabled") as f:
            return f.read() == "true" 
    except FileNotFoundError:
        with open("./dynamic/switching_enabled", "w") as f:
            f.write("true")
        with open("./dynamic/switching_enabled") as f:
            return f.read() == "true" 

def set_switch(allowed: bool):
    with open("./dynamic/switching_enabled", "w") as f:
        f.write("true" if allowed else "false")
